# доставка продуктов

import datetime


class Cart:
    def __init__(self, name, phone_number):
        self.name = name
        self.phone_number = phone_number
        cart = list()
        self.cart = cart
        cart_list = list()
        self.cart_list = cart_list
        total_sum = 0
        self.total_sum = total_sum

    def add_item(self, item, price, quantity):
        # добавление в корзину товаров в том порядке, в котором их добавляет пользователь,
        # а также складывание суммы покупок
        for i in range(quantity):
            self.cart.append([item, price])
            self.total_sum += price
        self.cart_list.clear()

    def ordered_cart(self):
        # список всех позиций в корзине, c количеством и ценой
        position_list = []
        for pos in self.cart:
            if pos[0] not in position_list:
                position_list.append(pos[0])
        for pos in position_list:
            counter = 0
            for item in self.cart:
                if pos == item[0]:
                    counter += 1
                    price = item[1]
            self.cart_list.append([pos, counter, price * counter])
        # print(self.cart_list)

    def del_item(self, item, price, quantity=1):
        # удаление из корзины и итоговой суммы нужных товаров
        counter = 0
        for pos in self.cart:
            if pos[0] == item:
                self.cart.remove(pos)
            counter += 1
            if counter == quantity:
                break
        self.total_sum -= price * quantity
        # очищаем, чтобы потом заново собрать всю общую информацию по покупкам
        self.cart_list.clear()

    def del_cart(self):
        # очистка корзины
        self.cart.clear()
        self.total_sum = 0
        self.cart_list.clear()


def payment():
    print('Сумма для оплаты', user_cart.total_sum)
    while True:
        pay = int(input('Введите сумму для оплаты: '))
        if pay == user_cart.total_sum:
            now = datetime.datetime.now().strftime("%H:%M:%S")
            print(f'Спасибо за покупку! {name}, ожидайте звонок опреатора на номер: {phone_number}\nВремя операции: {now}')
            break
        else:
            print('Cумма неверная, повторите ввод!')
            continue


list_dairy = [(1, 'Молоко', '-', 50), (2, 'Сливки', '-', 70), (3, 'Творог', '-', 100), (4, 'Сыр', '-', 250),
              (5, 'Сметана', '-', 55), (6, 'Яйца', '-', 80), (7, 'Йогурт', '-', 40), {'Назад', 0}]
list_meat = [(1, 'Говядина', '-', 300), (2, 'Курица', '-', 250), (3, 'Свинина', '-', 300), (4, 'Индейка', '-', 400),
             (5, 'Телятина', '-', 400), (6, 'Утка', '-', 450), (7, 'Баранина', '-', 500), {'Назад', 0}]
list_bakery = [(1, 'Батон', '-', 45), (2, 'Хлеб', '-', 40), (3, 'Торт', '-', 250), (4, 'Кекс', '-', 50),
               (5, 'Пирог', '-', 150), (6, 'Сушки', '-', 40), (7, 'Круассан', '-', 80), {'Назад', 0}]
list_grains = [(1, 'Геркулес', '-', 35), (2, 'Рис', '-', 55), (3, 'Гречка', '-', 70), (4, 'Макароны', '-', 65),
               (5, 'Пшено', '-', 45), (6, 'Мюсли', '-', 90), (7, 'Киноа', '-', 95), {'Назад', 0}]
list_sweets = [(1, 'Мармелад', '-', 60), (2, 'Шоколад', '-', 120), (3, 'Печенье', '-', 55), (4, 'Вафли', '-', 60),
               (5, 'Конфеты', '-', 155), (6, 'Леденец', '-', 30), (7, 'Джем', '-', 125), {'Назад', 0}]
list_fruits = [(1, 'Бананы', '-', 65), (2, 'Яблоки', '-', 45), (3, 'Апельсины', '-', 70), (4, 'Киви', '-', 90),
               (5, 'Манго', '-', 150), (6, 'Груша', '-', 80), (7, 'Ананас', 125), {'Назад', 0}]
list_vegetables = [(1, 'Огурцы', '-', 110), (2, 'Помидоры', '-', 90), (3, 'Картофель', '-', 40), (4, 'Цукини', '-', 60),
                   (5, 'Капуста', '-', 25), (6, 'Лук', '-', 20), (7, 'Морковь', '-', 55), {'Назад', 0}]


def category_and_products(category_number):
    f = lambda x: " ".join(map(str, x))
    dairy_product = (" руб.\n".join(f(x) for x in list_dairy))
    meat_product = (" руб.\n".join(f(x) for x in list_meat))
    bakery_product = (" руб.\n".join(f(x) for x in list_bakery))
    grains_product = (" руб.\n".join(f(x) for x in list_grains))
    sweets_product = (" руб.\n".join(f(x) for x in list_sweets))
    fruits_product = (" руб.\n".join(f(x) for x in list_fruits))
    vegetables_product = (" руб.\n".join(f(x) for x in list_vegetables))

    if category_number == 1:
        print(f"Выберите нужный продукт:\n{dairy_product}")
        product1 = input(f"{'-' * 20}\n : ")
        return product1
    elif category_number == 2:
        print(f"Выберите нужный продукт:\n{meat_product}")
        product1 = input(f"{'-' * 20}\n : ")
        return product1
    elif category_number == 3:
        print(f"Выберите нужный продукт:\n{bakery_product}")
        product1 = input(f"{'-' * 20}\n : ")
        return product1
    elif category_number == 4:
        print(f"Выберите нужный продукт:\n{grains_product}")
        product1 = input(f"{'-' * 20}\n : ")
        return product1
    elif category_number == 5:
        print(f"Выберите нужный продукт:\n{sweets_product}")
        product1 = input(f"{'-' * 20}\n : ")
        return product1
    elif category_number == 6:
        print(f"Выберите нужный продукт:\n{fruits_product}")
        product1 = input(f"{'-' * 20}\n : ")
        return product1
    elif category_number == 7:
        print(f"Выберите нужный продукт:\n{vegetables_product}")
        product1 = input(f"{'-' * 20}\n : ")
        return product1


def product_price(product1, category):  # Возвращает цену и название продукта
    if category == 1:
        price1 = list_dairy[int(product1) - 1][3]
        prodname = list_dairy[int(product1) - 1][1]
        return price1, prodname
    elif category == 2:
        price1 = list_meat[int(product1) - 1][3]
        prodname = list_meat[int(product1) - 1][1]
        return price1, prodname
    elif category == 3:
        price1 = list_bakery[int(product1) - 1][3]
        prodname = list_bakery[int(product1) - 1][1]
        return price1, prodname
    elif category == 4:
        price1 = list_grains[int(product1) - 1][3]
        prodname = list_grains[int(product1) - 1][1]
        return price1, prodname
    elif category == 5:
        price1 = list_sweets[int(product1) - 1][3]
        prodname = list_sweets[int(product1) - 1][1]
        return price1, prodname
    elif category == 6:
        price1 = list_fruits[int(product1) - 1][3]
        prodname = list_fruits[int(product1) - 1][1]
        return price1, prodname
    elif category == 7:
        price1 = list_vegetables[int(product1) - 1][3]
        prodname = list_vegetables[int(product1) - 1][1]
        return price1, prodname


def check_answer(answer):
    error = 0
    if answer.isalpha():
        error += 1
        return error
    try:
        if 7 < int(answer):
            error += 1
    finally:
        return error


def check_name(name):
    error = 0
    if name.isalpha() is True and len(name) > 1:
        error = 0
        return error
    else:
        error += 1
        return error


def check_phone(phone_number):
    error = 0
    if phone_number.isdigit() is True and len(phone_number) == 11:
        error = 0
        return error
    else:
        error += 1
        return error


def buying():
    output = 0
    while output == 0:
        print('Выберите нужную категорию:\n1 Молочная продукция\n2 Мясная продукция\n3 Выпечка\n4 Крупы\n5 Сладости'
              '\n6 Фрукты\n7 Овощи\n0 Назад')
        user_answer = input(f"{'-' * 20}\n : ")
        result = check_answer(user_answer)
        if result == 1:
            print("Введена несуществующая категория!")
            continue
        # работает возврат из продуктовых категорий
        elif user_answer.lower() == '0':
            break
        while True:
            user_product = category_and_products(int(user_answer))  # хранит число-ответ пользователя
            result = check_answer(user_product)
            if user_product == 1:
                print("Введен несуществующий продукт!")
                continue
            elif user_product.lower() == '0':
                break
            else:
                price, productuser = product_price(user_product, int(user_answer))
                count1 = input('Введите нужное количество. Максимальное количество товара - 7.(шт/кг): ')
                result = check_answer(count1)
                if result == 1:
                    print("Некорректное количество. Введите число!\nМаксимальное количество товара - 7")
                    continue
                user_cart.add_item(productuser, price, int(count1))
                user_cart.ordered_cart()
                print(user_cart.cart_list)
                print(f'Сумма заказа: {user_cart.total_sum} рублей')



userCorrect = False
while not userCorrect:
    name = input("Добро пожаловать в доставку продуктов!\n(Для выхода введите '0')\nПредставьтесь: ")
    result = check_name(name)
    if name == '0':
        break
    elif result == 1:
        print("Введите ваше настоящее имя!")
        continue
    else:
        userCorrect = True
    phoneCorrect = False
    while not phoneCorrect:
        phone_number = input("(Для выхода введите '0')\nОставьте свой контактный телефон: ")
        result = check_phone(phone_number)
        if phone_number == '0':
            break
        elif result == 1:
            print("Введите корректный номер телефона. Должен содержать 11 чисел(8...)!")
            continue
        else:
            phoneCorrect = True


user_cart = Cart(name, phone_number)

while True:
    print('Меню:\n'
          '1 Выбрать категорию продуктов\n'
          '2 Показать корзину\n'
          '3 Оплатить покупки\n'
          '4 Выход\n'
          '----------------------------')
    answer = int(input())
    if answer == 1:
        buying()
    elif answer == 2:
        print(user_cart.cart_list)
    elif answer == 3:
        payment()
        break
    elif answer == 4:
        print('До свидания! Приходите ещё!')
        break
